package model;

public class HouseInfo {
	private String name;
	private Long mobilenumber;
	private String address;
	private String location;
	private String housesize;
	private String bhk;
	private String facilities;
    private Integer id;
    public HouseInfo() {
		
	}
	public HouseInfo(String name, Long mobilenumber, String address, String location, String housesize, String bhk,
			String facilities, Integer id) {
		super();
		this.name = name;
		this.mobilenumber = mobilenumber;
		this.address = address;
		this.location = location;
		this.housesize = housesize;
		this.bhk = bhk;
		this.facilities = facilities;
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(Long mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getHousesize() {
		return housesize;
	}
	public void setHousesize(String housesize) {
		this.housesize = housesize;
	}
	public String getBhk() {
		return bhk;
	}
	public void setBhk(String bhk) {
		this.bhk = bhk;
	}
	public String getFacilities() {
		return facilities;
	}
	public void setFacilities(String facilities) {
		this.facilities = facilities;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}

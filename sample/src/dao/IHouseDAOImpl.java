package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;
import model.HouseInfo;
import util.DbUtil;
import util.QueryUtil;

public class IHouseDAOImpl implements IHouseDAO{
    int result;
    PreparedStatement pst;
    ResultSet rs;
	@Override
	public int adminLogin(Admin admin) {
		result=0;
		try {
		pst=DbUtil.getCon().prepareStatement(QueryUtil.adminLogin);
		pst.setString(1,admin.getUsername());
		pst.setString(2,admin.getPassword());
		rs=pst.executeQuery();
		while(rs.next()) {
			result++;
		}
		}catch (ClassNotFoundException | SQLException ex){
			
		}
		return result;
	}
	@Override
	public List<HouseInfo> viewAll() {	
		List<HouseInfo> list=new ArrayList <HouseInfo>();
		try {
			pst=DbUtil.getCon().prepareStatement(QueryUtil.viewAll);
			
			
			rs=pst.executeQuery();
			while(rs.next()) {
				HouseInfo houseinfo=new HouseInfo(rs.getString(1),rs.getLong("mobilenumber"),rs.getString(3),rs.getString(4), 
						rs.getString(5),rs.getString(6),rs.getString(7),rs.getInt(8));
				list.add(houseinfo);
			}
			}catch (ClassNotFoundException | SQLException ex){
				
			}
	return list;
	}
	@Override
	public void delete(HouseInfo houseinfo) {
	try {
		pst=DbUtil.getCon().prepareStatement(QueryUtil.delete);
		pst.setInt(1,houseinfo.getId());
		pst.executeUpdate();
	}catch (ClassNotFoundException | SQLException ex) {
		
	}
		
	}
	@Override
	public void update(HouseInfo houseinfo) {
		try {
			pst=DbUtil.getCon().prepareStatement(QueryUtil.update);
			pst.setString(1, houseinfo.getName());
			pst.setLong(2, houseinfo.getMobilenumber());
			pst.setString(3, houseinfo.getAddress());
			pst.setString(4, houseinfo.getLocation());
			pst.setString(5, houseinfo.getHousesize());
			pst.setString(6, houseinfo.getBhk());
			pst.setString(7, houseinfo.getFacilities());
			pst.setInt(8,houseinfo.getId());
			pst.executeUpdate();
		}catch (ClassNotFoundException | SQLException ex) {
			
		}
		
	}
	@Override
	public int add(HouseInfo houseinfo) {
		result=0;
		try {
			pst=DbUtil.getCon().prepareStatement(QueryUtil.add);
			pst.setString(1, houseinfo.getName());
			pst.setLong(2, houseinfo.getMobilenumber());
			pst.setString(3, houseinfo.getAddress());
			pst.setString(4, houseinfo.getLocation());
			pst.setString(5, houseinfo.getHousesize());
			pst.setString(6, houseinfo.getBhk());
			pst.setString(7, houseinfo.getFacilities());
			pst.setInt(8,houseinfo.getId());
			result=pst.executeUpdate();
		}catch (ClassNotFoundException | SQLException ex) {
			return result;
		}
		return result;
	}

}

package dao;

import java.util.List;

import model.Admin;
import model.HouseInfo;

public interface IHouseDAO {
	public int adminLogin(Admin admin);
    public List<HouseInfo> viewAll();
    public void delete(HouseInfo houseinfo);
    public void update(HouseInfo houseinfo);
    public int add(HouseInfo houseinfo);
}

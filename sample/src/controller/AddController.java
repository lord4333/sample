package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import dao.IHouseDAO;
import dao.IHouseDAOImpl;
import model.HouseInfo;


@WebServlet("/add")
public class AddController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		  String name=request.getParameter("name");
			Long mobilenumber=Long.parseLong(request.getParameter("mobilenumber"));
			 String address=request.getParameter("address");
			 String location=request.getParameter("location");
			 String housesize=request.getParameter("housesize");
			 String bhk=request.getParameter("bhk");
			 String facilities=request.getParameter("facilities");
			int id=Integer.parseInt(request.getParameter("id"));
			
			HouseInfo houseinfo=new HouseInfo(name,mobilenumber,address,location,housesize,bhk,facilities,id);
			IHouseDAO dao=new IHouseDAOImpl();
			int result=dao.add(houseinfo);
			if(result>0) {
				request.setAttribute("msg", id+ " Added Successfully");
			}else {
				request.setAttribute("msg", id+ " Dublicate Entry");
			}
			List<HouseInfo> list=dao.viewAll();
			request.setAttribute("hou", list);
			request.getRequestDispatcher("admincrud.jsp").forward(request, response);
			
	}

}

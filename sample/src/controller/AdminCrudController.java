package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.IHouseDAO;
import dao.IHouseDAOImpl;
import model.HouseInfo;
 
@WebServlet("/admincrud")
public class AdminCrudController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		 IHouseDAO dao=new IHouseDAOImpl();
			List<HouseInfo> list= dao.viewAll();
			request.setAttribute("hou",list);
			 request.getRequestDispatcher("admincrud.jsp").include(request, response);
	}

}

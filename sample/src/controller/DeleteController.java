package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.IHouseDAOImpl;
import model.HouseInfo;


@WebServlet("/delete")
public class DeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	Integer id=Integer.parseInt(request.getParameter("id"));
		IHouseDAOImpl dao= new IHouseDAOImpl();
		HouseInfo houseinfo=new HouseInfo();
		houseinfo.setId(id);
		
		dao.delete(houseinfo);
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		out.print(id+ "Deleted Successfully<br/>");
		List<HouseInfo> list=dao.viewAll();
		request.setAttribute("hou", list);
		request.getRequestDispatcher("admincrud").include(request,response);
	}

}

package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.IHouseDAO;
import dao.IHouseDAOImpl;
import model.Admin;
import model.HouseInfo;


@WebServlet("/login")
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		 String username=request.getParameter("username");
		 String password=request.getParameter("password");
		 
		 
		 Admin admin =new Admin(username,password);
		 IHouseDAO dao=new IHouseDAOImpl();
		 int result=dao.adminLogin(admin);
		 if(result>0) {
			List<HouseInfo> list= dao.viewAll();
			request.setAttribute("hou",list);
			 request.getRequestDispatcher("admin.jsp").forward(request, response);
		 }else {
			 request.setAttribute("error","Hi " +username+ " Please Check Your Credentials");
			 request.getRequestDispatcher("index.jsp").forward(request, response);	 
		 }
		 
	}

}

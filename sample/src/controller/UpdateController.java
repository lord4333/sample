package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.HouseInfo;


@WebServlet("/update")
public class UpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response)  
			throws ServletException, IOException {
    String name=request.getParameter("name");
	Long mobilenumber=Long.parseLong(request.getParameter("mobilenumber"));
	 String address=request.getParameter("address");
	 String location=request.getParameter("location");
	 String housesize=request.getParameter("housesize");
	 String bhk=request.getParameter("bhk");
	 String facilities=request.getParameter("facilities");
	int id=Integer.parseInt(request.getParameter("id"));
	
	HouseInfo houseinfo=new HouseInfo(name,mobilenumber,address,location,housesize,bhk,facilities,id);
	request.setAttribute("hou", houseinfo);
	request.getRequestDispatcher("update.jsp").forward(request, response);
	}

}

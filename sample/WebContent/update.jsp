<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update</title>
</head>
<body>

<h1>Admin Update Page</h1>
<form action="updatecont" method="post">
House Owner Name:<input type="text" name="name" value="${hou.getName()}" /><br/><br/>
Mobile Number:<input type="text" name="mobilenumber" value="${hou.getMobilenumber()}" /><br/><br/>
Address:<input type="text" name="address" value="${hou.getAddress()}" /><br/><br/>
Location:<input type="text" name="location" value="${hou.getLocation()}" /><br/><br/>
House size:<input type="text" name="housesize" value="${hou.getHousesize()}" /><br/><br/>
BHK:<input type="text" name="bhk" value="${hou.getBhk()}" /><br/><br/>
Facilities:<input type="text" name="facilities" value="${hou.getFacilities()}" /><br/><br/>
House Id:<input type="text" name="id" readonly="readonly" value="${hou.getId()}" /><br/><br/>
<input type="submit" value="update" />
</form>

</body>
</html>
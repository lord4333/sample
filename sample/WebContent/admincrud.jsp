<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Crud</title>
</head>
<body>

<h1>Admin Crud operations Page</h1>

<p align="right">
<a href="insert">Add New House Details</a>
<a href="index.jsp">Logout</a>
</p>
<hr />
${msg}
<table border="3">
<tr>
<th colspan="8">House Details</th>
</tr>
<tr>
<th>Name</th>
<th>MobileNumber</th>
<th>Address</th>
<th>Location</th>
<th>HouseSize</th>
<th>Bhk</th>
<th>Facilities</th>
<th>Id</th>
<th>Update</th>
<th>Delete</th>
</tr>
<c:forEach items="${hou}" var="h"> 
<tr style="text-align: centre;">

<td>${h.getName()}</td>
<td> ${h.getMobileNumber()}</td> 
<td>${h.getAddress()}</td> 
<td> ${h.getLocation()}</td> 
<td> ${h.getHousesize()}</td>
<td> ${h.getBhk()}</td>
<td> ${h.getFacilities()}</td>
<td> ${h.getId()}</td>
<td><a href="update?name=${h.getName()}&mobilenumber=${h.getMobileNumber()}&address=${h.getAddress()}
&location= ${h.getLocation()}&housesize= ${h.getHousesize()}&bhk=${h.getBhk()}&facilities=${h.getFacilities()}
&id=${h.getId()}">Update</a></td>
<td><a href="delete?id=${h.getId()}"?>Delete</a></td>
</tr>
</c:forEach>

</body>
</html>